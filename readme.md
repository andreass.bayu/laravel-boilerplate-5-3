## Laravel 5.4 Boilerplate

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate) [![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate)

### Installation

- This package ships with a __.env.example__ file in the root of the project. You must rename this file to just __.env__ Change database setting in __.env__ file as your setting
- `Composer install`
- `npm install` or `yarn`
- `php artisan key:generate`
- `php artisan migrate`
- `php artisan db:seed`
- `npm run`
- `phpunit`
- `composer dump-autoload`

### Login Account (seed)

You can change seed data for login account before migrating database in __database/seeds/access/__ folder

#### Admin (Administrator)

Email = admin@admin.com

Pass  = 1234

#### Executive (Can only view backend)

Email = executive@executive.com

Pass  = 1234

#### User (Can only access frontpage)

Email = user@user.com

Pass  = 1234


### Official Documentation

[Click here for the official documentation](http://laravel-boilerplate.com)


You can download the last stable build of Laravel 5.1 [here](https://github.com/rappasoft/laravel-5-boilerplate/tree/Legacy_5.1).

### Introduction

Laravel Boilerplate provides you with a massive head start on any size web application. It comes with a full featured access control system out of the box with an easy to learn API and is built on a Twitter Bootstrap foundation with a front and backend architecture. We have put a lot of work into it and we hope it serves you well and saves you time!

### Wiki

Please view the [wiki](https://github.com/rappasoft/laravel-5-boilerplate/wiki) for a list of [features](https://github.com/rappasoft/laravel-5-boilerplate/wiki#features).

### License

MIT: [http://anthony.mit-license.org](http://anthony.mit-license.org)